import React from 'react';
import { NavLink } from "react-router-dom";
import AppPopup from './AppPopup';

const FooterMenu = props => {
  return (
    <React.Fragment>
        <div className="copyright py-4 text-center text-white">
            <div className="container">
                <NavLink activeClassName="active" to="/rules">Rules</NavLink>
                <NavLink activeClassName="active" to="/tos">Terms of Service</NavLink>
                <NavLink activeClassName="active" to="/privacy">Privacy</NavLink>
                <NavLink activeClassName="active" to="/about">About</NavLink>
                <NavLink activeClassName="active" to="/contact">Contact</NavLink>
                <NavLink activeClassName="active" data-toggle="modal" data-target="#appModal" to="/app">App</NavLink>
            </div>
        </div>
        <div className="scroll-to-top d-lg-none position-fixed ">
            <a className="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
            <i className="fa fa-chevron-up"></i>
            </a>
        </div>
    <AppPopup />
    </React.Fragment>
  );
};


export default FooterMenu;
