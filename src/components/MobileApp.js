import React from 'react';

const MobileApp = props => {
    return(
        <React.Fragment>
            <p> DoubleSend App Available Soon!</p>
            <img src={require('../img/appstore.png')} />
            <img src={require('../img/googleplay.png')} />
        </React.Fragment>
    );
};

export default MobileApp;
