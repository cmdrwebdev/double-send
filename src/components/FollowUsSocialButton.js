import React from 'react';

const FollowUsSocialButton = props => {
    return(
        <React.Fragment>
            <p> Follow Us On</p>
            
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" target="_blank" href="https://www.facebook.com/doublesend/">
                            <img src={require('../img/facebook.png')} />
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" target="_blank" href="https://twitter.com/playdoublesend">
                            <img src={require('../img/twitter.png')} />
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" target="_blank" href="https://www.instagram.com/playdoublesend/">
                            <img src={require('../img/instagram.png')} />
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <img src={require('../img/youtube.png')} />        
                    </li>
                </ul>
            
            
            
            
        </React.Fragment>
    );
};

export default FollowUsSocialButton;