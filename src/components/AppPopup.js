import React from 'react';


const AppPopup = props => {
  return(
    <React.Fragment>
      <div className="modal fade" id="appModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" style={{maxWidth:'97%',width:'auto'}} role="document">
          <div className="modal-content">
            <div className="exitRow"><a href="#" id="exitButton"><img data-dismiss="modal" src={require('../img/exit.png')}/></a> </div>
            <div className="modal-body" >
              <div className="container">
                <div className="row">
                  <div className="col">
                    <img src={require('../img/ds-logo.png')}/>
                  </div>
                  <div className="col">
                    <h1>DOUBLESEND APP <br/> COMING SOON!</h1>
                      <div className="row">
                          <div className="col-sm-2"><img src={require('../img/appsmall.png')}/>  </div>
                          <div className="col-sm-2"><img src={require('../img/googlesmall.png')}/>  </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default AppPopup;
