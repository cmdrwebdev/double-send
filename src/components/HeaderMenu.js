import React, { useEffect, useRef } from 'react';
import { NavLink,Link } from "react-router-dom";


const HeaderMenu = props => {

    const homeLinkRef = useRef(null);

    useEffect(()=>{
         window.scrollTo(0, 0);
         if (props.location){
          if (props.location.pathname === '/welcome' || props.location.pathname === '/congratulations' || props.location.pathname === '/'){
            homeLinkRef.current.classList.add('active')
          }
        }
    },[])
    return (
        <nav className="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
            <div className="container">
                <a className="navbar-brand js-scroll-trigger" href="#page-top"> <img src={require('../img/logo.png')} /></a>
                <button className="navbar-toggler navbar-toggler-right text-uppercase text-white rounded" type="button"
                    data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                    Menu
                    <i className="fas fa-bars"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item mx-0 mx-lg-1">
                            <Link to="/" innerRef={homeLinkRef} className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger ">Home</Link>
                        </li>
                        <li className="nav-item mx-0 mx-lg-1">
                            <NavLink activeClassName="active" to="/signup" className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger">Sign-up</NavLink>
                        </li>
                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" target="_blank" href="https://www.facebook.com/doublesend/"> <img
                                src={require('../img/facebook-square.png')} /> </a>
                        </li>

                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" target="_blank" href="https://twitter.com/playdoublesend"> <img src={require('../img/twittersquare.png')} />
                            </a>
                        </li>

                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" target="_blank" href="https://www.instagram.com/playdoublesend/"> <img
                                src={require('../img/instagramsquare.png')} /> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};


export default HeaderMenu;
