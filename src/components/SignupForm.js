import React from 'react';


const SignupForm = props => {
    return(
        <div>
            <div className="SIGN-UP-TO-WIN-CASH">
                SIGN-UP TO WIN CASH! <span className="text-style-1">it’s EASY!</span>
            </div>
            <form action="/congratulations" method="GET">
                FIRST NAME:
                <input type="text" name="first_name"/><br/>
                MIDDLE NAME:
                <input type="text" name="middle_name" /><br/>
                LAST NAME:
                <input type="text" name="last_name" /><br/>
                EMAIL:
                <input type="text" name="email" /><br/>
                PASSWORD:
                <input type="password" name="password" /><br/>
                CONFIRM PASSWORD:
                <input type="password" name="confirm_password" /><br/>
                MOBILE PHONE:
                <input type="text" name="mobile phone" /><br/>
                <input type="checkbox" checked /><a href="#">I agree to the Terms of Service</a><br/>
                <input type="submit" value="FINISH" />
            </form>
        </div>
    );
};

export default SignupForm;