import axios from 'axios';


const auth = () => {
  const url = 'http://dev.doublesend.com/auth/signin';

  axios.post(url, {"username":"dsadmin","password":"d0ubl3s3nd"}, {
    headers: {
      'Content-Type': 'application/json',
    }
  }).then(function(response) {
    console.log('Authenticated' + response.data.token);
  }).catch(function(error) {
    console.log('Error on Authentication' + error);
  });
}

const login = (email,password) => {

  const url = 'http://dev.doublesend.com/api/users/login?email=test@yahoo.com&pwd=banana1';

  axios.post(url, {"email":"test@yahoo.com","pwd":"banana1"}, {
    headers: {
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJkc2FkbWluIiwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9BRE1JTiJdLCJpYXQiOjE1NTc1ODE3ODksImV4cCI6MTU1NzU4NTM4OX0.AcDIIaoxahcIc3GXPZEUNDhQJoF9gRqLs9ESGTGgz6c',
      'Access-Control-Allow-Origin':'*'
    }
  }).then(function(response) {
    console.log('Authenticated' + response);
  }).catch(function(error) {
    console.log('Error on Authentication' + error);
  });
}

export const userService = {
    auth,
    login,
    sendEmail
};
