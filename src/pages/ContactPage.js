import React, { useEffect,useContext,useState } from 'react';
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';
import RootContext from '../context/root-context';

const ContactPage = props => {
  const context = useContext(RootContext);
  const [to, setTo] = useState();
  const [subject, setSubject] = useState();
  const [message, setMessage] = useState();

  useEffect(()=>{
    if (context.isEmailSuccess){
      setTo("");
      setSubject("");
      setMessage("");
    }
  },[context.isRequesting])

  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead  text-white text-center">
        <div className="about contact">
          <h1 className="textleft contacth1 ">CONTACT</h1>
          {
            !context.isRequesting && context.isEmailSuccess &&
            <div className="textcenter alert alert-success col-sm-5 offset-1" role="alert">
              Email successfully sent!
            </div>

          }
          {
            !context.isRequesting && !context.isEmailSuccess  && context.error_message &&
            <div className="textcenter alert alert-danger col-sm-5 offset-1" role="alert">
              Error sending email!
            </div>

          }
          <form className="textright signup-form">
            <div className="form-group row ">

              <label className="col-sm-1 col-form-label">EMAIL</label>
              <div className="col-sm-5">
                <input type="text" onChange={(e)=> setTo(e.target.value) } className="form-control" />
              </div>

            </div>
            <div className="form-group row ">
              <label className="col-sm-1 col-form-label">SUBJECT</label>
              <div className="col-sm-5">
                <input type="text" onChange={(e)=> setSubject(e.target.value) } className="form-control" />
              </div>

            </div>

            <div className="form-group row ">
              <label className="col-sm-1 col-form-label">MESSAGE</label>
              <div className="col-sm-5">
                <textarea onChange={(e)=> setMessage(e.target.value) }></textarea>
              </div>

            </div>
            <div className="col-sm-1 ob">
              <button type="button" onClick={()=> context.sendEmail(to,subject,message) } className="orangebutton"> SEND</button>
            </div>
          </form>
        </div>

      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default ContactPage;
