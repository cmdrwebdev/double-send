import React, { useEffect, useState,useContext,useReducer } from 'react';
import { Redirect } from "react-router-dom";
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';
import SigninForm from '../components/SigninForm';
import RootContext from '../context/root-context';



const SigninPage = props => {

  const context = useContext(RootContext);
  const [email, setEmail] = useState("");
  const [errorClass, setErrorClass] = useState("textleft invalid-feedback");
  const [password, setPassword] = useState("");



  useEffect(()=>{
    if (context.isLogin) {
        props.history.push('/welcome')
    }
  },[context.isLogin]);
  useEffect(()=>{
    if (context.error_message) {
      setErrorClass("textleft invalid-feedback d-block");
    }
  },[context.error_message]);
  return (

    <React.Fragment>

      <HeaderMenu />
      <header className="masthead text-white text-center">

        <div className="row signin">
          <div className="col-4">

            <img src={require('../img/ds-logo.png')} className="mainlogo" />

          </div>
          <div className="col-8">

            <form className="textright signup-form" action="/welcome">

              <div className="form-group row checkbox">
                <label className="col-sm-3 col-form-label">EMAIL </label>
                <div className="col-sm-5">
                  <input type="text" value={email} onChange={(e) => setEmail(e.target.value) } className="form-control" required />
                  <div className={errorClass}>
                    {context.error_message}
                  </div>
                </div>

              </div>
              <div className="form-group row checkbox">
                <label className="col-sm-3 col-form-label">PASSWORD</label>
                <div className="col-sm-5">
                  <input type="password" value={password} onChange={(e) => setPassword(e.target.value) } className="form-control" />
                </div>
              </div>

              <div className="form-group row checkbox">
                <label className="col-sm-3 col-form-label"></label>
                <div className="col-sm-5">
                  <button type="button" onClick={() => context.userLogin(email,password)} className="orangebutton"> SIGN IN</button>
                </div>
              </div>
            </form>
          </div>
        </div>

        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>
      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default SigninPage;
