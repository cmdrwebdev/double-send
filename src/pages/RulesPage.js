import React from 'react';
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';

const RulesPage = props => {
  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead  text-white text-center">

        <div className="about">
          <h1 className="textleft"> RULES</h1><br/>
          <span>DoubleSend Remittance Club is open to anyone who sends or receives money from a friend, loved one or family member.</span>
          <span>Rules pertaining to the soon-to-be-released app and the games that go with it will be posted with the app’s release.</span>
        </div>


        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>


      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default RulesPage;
