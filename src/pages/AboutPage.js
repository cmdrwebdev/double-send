import React from 'react';
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';


const AboutPage = props => {
  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead bg-primary text-white text-center">
        <div className="about">
          <h1 className="textleft"> ABOUT</h1><br/><br/>
          <span>DoubleSend is the new social network for you if you send money to a loved one or family member in your home country. It’s also your new social network if you receive money from a loved one or family member working overseas or in another city in your country. </span><br/><br/>
          <span>Sign up NOW and become part of it - DON’T JUST SEND MONEY - SEND LOVE TOO!.</span><br/><br/>
          <span>COMING SOON: Apps for your phone and fun games with the chance to win cash and prizes!</span><br/>
        </div>
        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>
      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default AboutPage;
