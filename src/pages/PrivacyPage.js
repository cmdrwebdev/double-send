import React from 'react';
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';


const PrivacyPage = props => {
  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead bg-primary text-white text-center">
        <div className="tos">
          <h1 className="textleft">DOUBLESEND PRIVACY STATEMENT</h1><br/><br/>
          <span>This DOUBLESEND Privacy Statement ("DoubleSend Privacy Statement" or "Privacy Statement")
explains DoubleSend’s use and disclosure of personally identifiable information that participants in the
DoubleSend Remittance Club (collectively, "Members") The term "User" refers to a Member or a Visitor.
This Privacy Statement is effective as of September 1, 2019.</span><br/><br/>
          <span>DoubleSend gathers certain types of information about its Users and their content. The following discloses
our information gathering and dissemination practices. Please review the following statement so you will
understand how the information we collect may be treated and used.</span><br/><br/>
        <ol>
          <li><b>Collection, Use, and Disclosure of Member Data</b></li>
          <span>DoubleSend will collect only an email address and a Member's chosen password as part of a Member’s
initial registration. After the Member registers for and begins using the applicable Service, DoubleSend
may collect information that the Member posts, submits, or uploads.</span><br/><br/>
          <span>DoubleSend may use Member data to confirm or fulfill a request that you make; to communicate with you
about services that DoubleSend provides (“Service”); to improve the Service; or for web site
administration, analysis, research or other internal purposes.</span><br/><br/>
<span>DoubleSend may disclose Member data (i) when we believe in good faith that disclosure is reasonably
necessary to comply with legal process as described below in this paragraph, (ii) to enforce the terms and
provisions of any agreements with respect to which you and DoubleSend are a party, (iii) to protect the
rights, property or personal safety of DoubleSend, its Members or the public; and (iv) to enforce
DoubleSend's Terms of Use.</span><br/><br/>
<span>DoubleSend may disclose Member data to comply with legal process. For example, DoubleSend complies
with lawful requests, such as subpoenas, court orders, or applicable laws, originating from or arising under
the courts or laws of the United States, including U.S. federal, state, and territorial courts and laws. In
complying with such requests, DoubleSend may be required to disclose Member data. We do not reveal
information until we have a good faith belief that an information request by law enforcement meets
applicable legal standards.</span><br/><br/>
<span>DoubleSend may disclose Member data to third-party contractors or vendors who are employed by or work
for DoubleSend in order to perform services for DoubleSend or for its Members (including without
limitation to process credit card payments, if any, or for web site technical or administration services)
("Third Party Contractors"). DoubleSend requires its Third Party Contractors to agree to comply with the
terms of this Privacy Statement with respect to any Member data that we disclose to them, and to use such
information only for the purposes for which they have been retained or hired.</span><br/><br/>
<span>If you publish Member data on any web site hosted by DoubleSend, you grant permission for DoubleSend
to disclose such Member data to the third parties to the same extent and to the same audience that you
allow to view such content or data.</span><br/><br/>
          <li><b>Collection, Use, and Disclosure of Aggregated Member Content</b></li>
          <span>DoubleSend automatically gathers certain information about all its Members and Visitors. We analyze the
content in order to understand aggregate User behaviors, aggregate trends, demographics, and to monitor
system performance. We may use this data to serve advertising and marketing materials relevant to you.</span><br/><br/>
          <li><b>Cookies, Web Beacons, and IP Address Logs</b></li>
          <span>DoubleSend uses cookies. Cookies help us better understand and improve areas of the site that our Users
find valuable. If you have set your browser to warn you before accepting cookies, you will receive the
warning message with each cookie. Although parts of the DoubleSend Service may require the use of
cookies, Users always have the option of disabling cookies via their browser preferences. You can refuse
cookies by turning them off in your browser. However, parts of the Service may not work if you have
cookies disabled.</span><br/><br/>
<span>DoubleSend may also use web beacons, which are also known as pixel tags, Internet tags, clear GIFs, and
single-pixel GIFs). These are electronic images embedded into a web page. In addition, DoubleSend may
log Internet Protocol (IP) addresses – the location of your computer on the Internet.</span><br/><br/>
<li><b>Third Party Advertising</b></li>
<span>Third party advertisements displayed on the Service may also contain cookies set by Internet advertising
companies or advertisers. DoubleSend does not have access to or control over these third party cookies,
and users of the Service should check the privacy policy of the Internet advertising company or advertiser
to see whether and how it uses cookies</span><br/><br/>
<li><b>Member Feedback</b></li>
<span>DoubleSend encourages its Members to provide us with feedback and especially suggestions. We want to
make our offerings as useful and enjoyable as possible. We will never be able to figure out everything our
Members want, so we rely on you, our Member, to ask for new features and functions. Please understand
that all Member feedback and suggestions shall be deemed non-confidential and once submitted shall be
the sole property of DoubleSend. Without limitation to the foregoing, by providing any such material, you
hereby grant DoubleSend an unrestricted, irrevocable, royalty-free and perpetual right to freely reproduce,
use, disclose, modify, perform, publish, translate, adapt, create derivative works from, distribute and
display any such information you send DoubleSend, without limitation, for any and all commercial and
non-commercial purposes. DoubleSend is free to use any ideas, concepts or techniques contained in
Member feedback or suggestions for any purpose whatsoever including, but not limited to, developing,
manufacturing, and marketing products incorporating or adapting such ideas, concepts or techniques.</span><br/><br/>
<li><b>Changes to the Privacy Statement</b></li>
<span>If DoubleSend changes this Privacy Statement, we will post those changes on this web page, so that you
will always know what information we gather, how we might use that information, and whether we will
disclose it to anyone. By using the Service, you signify your assent to the DoubleSend Privacy Statement.
If you do not agree to this statement, please do not use the Service. Your continued use of the Service
following the posting of changes to these terms will mean you accept those changes.</span><br/><br/>
<li><b>Updating Member Data</b></li>
<span>Members have a duty to promptly update the email address(es) you provide to DoubleSend by logging in
and updating your information.</span><br/><br/>
<li><b>Security</b></li>
<span>DoubleSend uses commercially reasonable measures to protect Member Data. Despite these measures,
DoubleSend cannot guarantee that the Member Data that you submit will not be intercepted or otherwise
disclosed.</span><br/><br/>
<li><b>Contact Information</b></li>
<span>If you have any questions about the DoubleSend Privacy Statement or this web site, please contact:</span><br/><br/>
<span>DOUBLESEND</span><br/>
<span>PMB 542, Box 10,000</span><br/>
<span>Saipan, MP 96950</span><br/>
<span>info@doublesend.com</span><br/>

        </ol>
        </div>
        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>
      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default PrivacyPage;
