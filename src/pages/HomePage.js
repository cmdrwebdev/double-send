import React,{ useEffect, useContext } from 'react';
import ImageGallery from 'react-image-gallery';
import { Link } from "react-router-dom";
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import RootContext from '../context/root-context';
import "../../node_modules/react-image-gallery/styles/css/image-gallery.css";

const HomePage = props => {

  const context = useContext(RootContext);
  const images = [
    {
      original: require('../img/slide1.jpg')
    },
    {
      original: require('../img/slide2.jpg')
    },
    {
      original: require('../img/slide3.jpg')
    },
    {
      original: require('../img/slide4.jpg')
    },
    {
      original: require('../img/slide5.jpg')
    }
  ]

  useEffect(() => {
    console.log(context.user)
    if (!context.isLogin) {
      props.history.push('/');
    }
  },[context.isLogin]);

  const joinNow = () => {
    props.history.push('/signup');
  }
  const howItWorks = () => {
    props.history.push('/how-it-works');
  }
  return (
    <React.Fragment>
      <HeaderMenu {...props} />
      <header className="masthead  text-white text-center">
        <div className="row">
          <div className="col-4">
            <img src={require('../img/ds-logo.png')} className="mainlogo r" />
          </div>
          <div className="col-8">
            <div className="membersignin">
              <p> Member {!context.isLogin ? <Link to="/signin">Sign In</Link>: <a href="#" onClick={()=>{context.userLogout()}}>Logout</a>}</p>

            </div>
            <h1 className="textleft trs">REMITTANCE <br/> CLUB</h1>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col">
              <p className="home-normal-text"> Do you Send or Receive Money?</p>
              <div className="form-group">
                <button type="submit" onClick={ ()=> joinNow() } className="orangebutton"> JOIN NOW!</button>
                <p className="home-normal-text" style={{paddingTop:'20px'}}> Win Cash!</p>
                <h1 className="yellow"> IT'S FREE!</h1>
              </div>
              <div class="howitworksdiv"><a href="" onClick={ ()=> howItWorks() } className="howitworks" data-toggle="modal" data-target="#hiwModal" > How it Works</a></div>
            </div>
            <div className="col winner-section">
              LATEST WINNERS
              <div className="frame">
                <ImageGallery showThumbnails={false} showNav={false} showFullscreenButton={false} slideDuration={900} slideInterval={5000} showPlayButton={false} disableArrowKeys={true} showPlayButton={false} autoPlay={true} items={images} />
              </div>
            </div>
          </div>
        <h1 className="title textleft whitetext" style={{paddingTop:'20px', paddingBottom:'20px'}}> DON'T JUST SEND MONEY! SEND LOVE TOO!</h1>
        </div>
    </header>
    <FooterMenu />
  </React.Fragment>
  );
};

export default HomePage;
