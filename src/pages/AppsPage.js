import React from 'react';
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';

const AppsPage = props => {
  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead bg-primary text-white text-center">
        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>
      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default AppsPage;