import React,{useEffect} from 'react';
import ImageGallery from 'react-image-gallery';
import "../../node_modules/react-image-gallery/styles/css/image-gallery.css";
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';

const CongratulationsPage = props => {
  const images = [
    {
      original: require('../img/slide1.jpg')
    },
    {
      original: require('../img/slide2.jpg')
    },
    {
      original: require('../img/slide3.jpg')
    },
    {
      original: require('../img/slide4.jpg')
    },
    {
      original: require('../img/slide5.jpg')
    }
  ]
  return (
    <React.Fragment>
      <HeaderMenu {...props} />
      <header className="masthead bg-primary text-white text-center congratulations">

        <div className="row">
          <div className="col-5">
            <img src={require('../img/ds-logo.png')} className="mainlogo" />
          </div>
          <div className="col-7">
            <div className="col winner-section">
              MORE WINNERS EACH DAY
              <div className="frame">
                <ImageGallery showThumbnails={false} showNav={false} showFullscreenButton={false} slideDuration={900} slideInterval={5000} showPlayButton={false} disableArrowKeys={true} showPlayButton={false} autoPlay={true} items={images} />
              </div>
            </div>
          </div>
        </div>

        <h1 className="p20"> CONGRATULATIONS!</h1>
        <h2> YOU ARE SUCCESSFULLY <br /> ENTERED IN THE CASH GIVEAWAY!</h2>

        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>
      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default CongratulationsPage;
