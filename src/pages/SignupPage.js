import React,{ useEffect, useState, useContext,useReducer,useRef } from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import ImageGallery from 'react-image-gallery';
import "../../node_modules/react-image-gallery/styles/css/image-gallery.css";
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import {ShowSlides} from '../js/main.js';
import RootContext from '../context/root-context';


const SignupPage = props => {
  const context = useContext(RootContext);
  const [errorClass, setErrorClass] = useState("textleft invalid-feedback");
  const [agreeErrorClass, setAgreeErrorClass] = useState("textleft invalid-feedback");
  const [passwordError, setPasswordError] = useState("This field is required");
  const [passwordErrorClass, setPasswordErrorClass] = useState("textleft invalid-feedback");
  const [recaptcha, setRecaptcha] = useState();
  const [recaptchaErrorClass, setRecaptchaErrorClass] = useState("textleft invalid-feedback");
  const [user, setUser] = useState({});
  const form = useRef(null);
  const email = useRef(null);
  const password = useRef(null);
  const passwordConfirm = useRef(null);
  const agreeCheck = useRef(null);

  const images = [
    {
      original: require('../img/slide1.jpg')
    },
    {
      original: require('../img/slide2.jpg')
    },
    {
      original: require('../img/slide3.jpg')
    },
    {
      original: require('../img/slide4.jpg')
    },
    {
      original: require('../img/slide5.jpg')
    }
  ]

  const termsOfServicePage = () => {
    props.history.push('/tos');
  }
  
  useEffect(() => {
    console.log(user);
  },[user]);
  useEffect(() => {
    if (context.isRegisterSuccess) {
      props.history.push('/congratulations')
    }
  },[context.isRegisterSuccess]);

  useEffect(()=>{
    if (context.error_message) {
      setErrorClass("textleft invalid-feedback d-block");
    }
  },[context.error_message]);

  const handleOnBlur = (e) => {
    const input = e.target
    if (form.current.elements[input.name + "_check"] !== undefined){
      if(input.value != ""){
        form.current.elements[input.name + "_check"].checked = true
      }
    }
  }
  const handleForm = () => {
    var errorCount = 0

    if (agreeCheck.current.checkValidity()){
      setAgreeErrorClass("textleft invalid-feedback");
    }else{
      errorCount++;
      setAgreeErrorClass("textleft invalid-feedback d-block");
    }


    if(password.current.value == ""){
      errorCount++;
      setPasswordErrorClass("textleft invalid-feedback d-block");
    }else if (password.current.value != passwordConfirm.current.value) {
      errorCount++;
      setPasswordError("Password don't match");
      setPasswordErrorClass("textleft invalid-feedback d-block");
      form.current.elements["password_check"].checked = false
      form.current.elements["password_confirm_check"].checked = false
    }else{
      setPasswordErrorClass("textleft invalid-feedback");
    }

    if (recaptcha){
      setRecaptchaErrorClass("textleft invalid-feedback");
    }else{
      errorCount++
      setRecaptchaErrorClass("textleft invalid-feedback d-block");
    }

    if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.current.value)))
    {
      errorCount++;
      setErrorClass("textleft invalid-feedback d-block");
      form.current.elements["email_check"].checked = false
    }else{
      setErrorClass("textleft invalid-feedback");
    }
    console.log(errorCount)
    if(errorCount === 0){
      context.register(user)
    }
  }
  const onChange = (value) => {
    setRecaptcha(value)
  }
  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead  text-white text-center su">
        <div className="container">
          <div className="row">
            <div className="col mainlogosignup">
              <img src={require('../img/ds-logo.png')} className="mainlogo signup" />
            </div>
            <div className="col winner-section r">
                LATEST WINNERS
                <div className="frame">
                  <ImageGallery showThumbnails={false} showNav={false} showFullscreenButton={false} slideDuration={900} slideInterval={5000} showPlayButton={false} disableArrowKeys={true} showPlayButton={false} autoPlay={true} items={images} />
                </div>
            </div>
          </div>
          <div className="row">
            <p className="signuptowincash"> SIGN UP TO WIN CASH <br /> <span style={{color:'#fff'}}> IT'S FREE</span></p>
            <div className="signupform" >
              <form ref={form} className="textright signup-form" id="signup-form" >
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">First Name</label>
                  <div className="col-sm-5">
                    <input type="text" name="first_name" value={user.first_name} onBlur={(e) => handleOnBlur(e)} id="firstName" onChange={(e) => setUser({...user,first_name:e.target.value})} className="form-control" />
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="first_name_check" value="check3" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">Middle Name</label>
                  <div className="col-sm-5">
                    <input type="text" name="middle_name" value={user.middle_name} onBlur={(e) => handleOnBlur(e)} onChange={(e) => setUser({...user,middle_name:e.target.value})} className="form-control" />
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="middle_name_check" value="check2" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">Last Name</label>
                  <div className="col-sm-5">
                    <input type="text" name="last_name" value={user.last_name} onBlur={(e) => handleOnBlur(e)} onChange={(e) => setUser({...user,last_name:e.target.value})} className="form-control" />
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="last_name_check" value="check3" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">Email</label>
                  <div className="col-sm-5">
                    <input type="email" name="email" ref={email} value={user.email} onBlur={(e) => handleOnBlur(e)} onChange={(e) => setUser({...user,email:e.target.value})} className="form-control" required />
                    <div className={errorClass}>
                      {context.error_message || "Please provide a valid email."}
                    </div>
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="email_check" value="check3" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">Password</label>
                  <div className="col-sm-5">
                    <input type="password" ref={password} name="password" value={user.password} onBlur={(e) => handleOnBlur(e)} onChange={(e) => setUser({...user,password:e.target.value})} className="form-control" required />
                    <div class={passwordErrorClass}>
                      {passwordError}
                    </div>
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="password_check" value="check3" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">Confirm Password</label>
                  <div className="col-sm-5">
                    <input type="password" ref={passwordConfirm} name="password_confirm" value={user.confirm_password} onBlur={(e) => handleOnBlur(e)} onChange={(e) => setUser({...user,confirm_password:e.target.value})} className="form-control" required />
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="password_confirm_check" value="check3" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="form-group row checkbox">
                  <label className="col-sm-4 col-form-label">Mobile Phone</label>
                  <div className="col-sm-2">
                    <select id="inputState" value={user.mobile_prefix} onBlur={(e) => handleOnBlur(e)}  onChange={(e) => setUser({...user,mobile_prefix:e.target.value})} className="form-control dropdown">
                      <option selected ></option>
                      <option value="+63" >+63</option>
                      <option value="+1">+1</option>
                    </select>
                  </div>

                  <div className="col-sm-3">
                    <input type="text" value={user.mobile} name="mobile" onBlur={(e) => handleOnBlur(e)} onChange={(e) => setUser({...user,mobile:e.target.value})} className="form-control" style={{width:'167px'}}/>
                  </div>
                  <div class="col-sm-3">
                       <input id="check3" type="checkbox" name="mobile_check" value="check3" />
                       <label for="check3" class="earn"></label>
                 </div>
                </div>
                <div className="col-sm-11 agree_checkbox textleft">
                  <input id="check13" ref={agreeCheck} type="checkbox" name="check" value="check13" required />
                  
                  <label for="check13" className="earn"><a href="" onClick={ ()=> termsOfServicePage() } data-toggle="modal" data-target="#hiwModal" >I agree to the Terms of Service</a></label>
                  <div class={agreeErrorClass}>
                    You must agree before submitting.
                  </div>
                </div>
                <br/>
                <div className="col-sm-7 checkbox textleft">
                  <ReCAPTCHA
                    sitekey="6LeLNaIUAAAAAGWytW9ElBS6IkWI0Y6_itCTA392"
                    onChange={onChange}
                    required
                  />
                  <div class={recaptchaErrorClass}>
                    Invalid captcha.
                  </div>
                </div>
                <br/>
                <div className="form-group row">
                  <div className="col-sm-10">
                    <button type="button" onClick={()=> {handleForm()}} className="orangebutton"> FINISH</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </header>
      <FooterMenu />
  </React.Fragment>
  );
};


export default SignupPage;
