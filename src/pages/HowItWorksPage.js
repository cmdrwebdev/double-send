import React from 'react';
import HeaderMenu from '../components/HeaderMenu';
import FooterMenu from '../components/FooterMenu';
import FollowUsSocialButton from '../components/FollowUsSocialButton';
import MobileApp from '../components/MobileApp';


const HowItWorksPage = props => {
  return (
    <React.Fragment>
      <HeaderMenu />
      <header className="masthead bg-primary text-white text-center">
        <div className="about">
          <h1 className="textleft"> How it Works</h1><br/><br/>
          <span>DoubleSend is the social network that makes sending and receiving money more fun! </span><br/><br/>
          <span>Sign up NOW and become part of it - DON’T JUST SEND MONEY - SEND LOVE TOO!.</span><br/><br/>
          <span>To sign up:</span>
          <ol>
            <li>Enter your email</li>
            <li>Create a password</li>
          </ol>
          <span>Each individual is only allowed to sign up one time.</span><br/>
          <span>COMING SOON: Apps for your phone and fun games with the chance to win cash and prizes!</span><br/>
        </div>
        <div className="followuson">
          <FollowUsSocialButton />
          <MobileApp />
        </div>
      </header>
      <FooterMenu />
    </React.Fragment>
  );
};


export default HowItWorksPage;
