import React, { useState, useReducer } from 'react';
import axios from 'axios';
import RootContext from './root-context';
import { rootReducer,
         RESET,
         USER_LOGOUT,
         USER_LOGIN,
         USER_LOGIN_SUCCESS,
         USER_LOGIN_FAILED,
         USER_REGISTER,
         USER_REGISTER_SUCCESS,
         USER_REGISTER_FAILED,
         IS_REQUESTING,
         IS_SUCCESS,
         SEND_EMAIL
       } from './reducers';

const GlobalState = props => {


  const [state, dispatch] = useReducer(rootReducer, { isRequesting:false,isSuccess:false });

  const userLogin = (email,password) => {
    console.log("email" + email)
    console.log("password" + password)

    const url = 'https://dev.doublesend.com/auth/signin';
    dispatch({type: IS_REQUESTING, flag: true});
    axios.post(url, {"username":"dsadmin","password":"d0ubl3s3nd"}, {
      headers: {
        'Content-Type': 'application/json',
      }
    }).then(function(response) {

      console.log('Authenticated' + response.data.token);
      const url = 'https://dev.doublesend.com/api/users/login?email='+ email + '&pwd='+ password + '';

      axios.post(url, {"email":email,"pwd":password}, {
        headers: {
          'Authorization': 'Bearer ' + response.data.token,
        }
      }).then(function(response) {
        dispatch({type: IS_REQUESTING, flag: false});
        console.log('Authenticated /api/users/login ' + JSON.stringify(response.data));
        if (response.data.errCode === "0"){
          dispatch({ type: USER_LOGIN, user: response.data.object.firstName });
        }else{
          console.log(response.data.errMessage);
          dispatch({ type: USER_LOGIN_FAILED, message: response.data.errMessage });
        }


      }).catch(function(error) {
        dispatch({type: IS_REQUESTING, flag: false});
        console.log('Error on Authentication /api/users/login ' + error);
      });
    }).catch(function(error) {
      dispatch({type: IS_REQUESTING, flag: false});
      console.log('Error on Authentication' + error);
    });
  };

  const userLogout = () => {
    setTimeout(() => {
      dispatch({type: USER_LOGOUT});
    }, 700);
  };
  const register = (user) => {
    console.log("first_name " + user.first_name)
    console.log("middle_name " + user.middle_name)
    console.log("last_name " + user.last_name)
    console.log("email " + user.email)
    console.log("password " + user.password)
    console.log("confirm_password " + user.confirm_password)
    console.log("mobile " + user.mobile_prefix + user.mobile)

    const url = 'https://dev.doublesend.com/auth/signin';
    dispatch({type: IS_REQUESTING, flag: true});
    axios.post(url, {"username":"dsadmin","password":"d0ubl3s3nd"}, {
      headers: {
        'Content-Type': 'application/json',
      }
    }).then(function(response) {

      console.log('Authenticated' + response.data.token);
      const url = 'https://dev.doublesend.com/api/register';

      const param = {
        "firstName" : user.first_name || "",
        "middleName" : user.middle_name || "",
        "lastName" : user.last_name || "",
        "password" : user.password,
        "emailAddress":user.email,
        "phone" : user.mobile_prefix || "" + user.mobile || "",
        "citizenship" : "Filipino",
        "country" : "ph",
        "company" : "xei",
        "photoHref" : "",
        "notification" : "false",
        "activated" : "true"
      }
      axios.post(url, param, {
        headers: {
          'Authorization': 'Bearer ' + response.data.token,
        }
      }).then(function(response) {
        dispatch({type: IS_REQUESTING, flag: false});
        console.log('Authenticated /api/register ' + JSON.stringify(response.data));
        if (response.data.errCode === "0"){
          console.log("REGISTER SUCCESS")
          dispatch({ type: USER_REGISTER_SUCCESS,message: ""});
        }else{
          console.log("REGISTER FAILED " + response.data.errMessage)
          dispatch({ type: USER_REGISTER_FAILED,message: response.data.errMessage});
        }
      }).catch(function(error) {
        dispatch({type: IS_REQUESTING, flag: false});
        dispatch({ type: USER_REGISTER_FAILED});
        console.log('Error on Authentication /api/register ' + error);
      });
    }).catch(function(error) {
      dispatch({type: IS_REQUESTING, flag: false});
      console.log('Error on Authentication' + error);
    });
  };

  const reset = () => {
    dispatch({type: RESET});
  }

  const sendEmail = (to, subject, message) => {
    const url = 'https://dev.doublesend.com/auth/signin';
    dispatch({type: IS_REQUESTING, flag: true});
    axios.post(url, {"username":"dsadmin","password":"d0ubl3s3nd"}, {
      headers: {
        'Content-Type': 'application/json',
      }
    }).then(function(response) {

      console.log('Authenticated' + response.data.token);
      const url = 'https://dev.doublesend.com/api/sendmail?to=' + to + '&subj=' + subject+ '&message=' + message+ '';

      axios.get(url, {
        headers: {
          'Authorization': 'Bearer ' + response.data.token,
        }
      }).then(function(response) {
        dispatch({type: IS_REQUESTING, flag: false});
        console.log('Authenticated /api/sendmail ' + JSON.stringify(response.data));
        if (response.data.errCode === "0"){
          dispatch({ type: SEND_EMAIL, flag: true, message: "" });
        }else{
          dispatch({ type: SEND_EMAIL, flag: false, message: "Error sending email!" });
        }

      }).catch(function(error) {
        dispatch({type: IS_REQUESTING, flag: false});
        console.log('Error on /api/sendmail ' + error);
      });
    }).catch(function(error) {
      dispatch({type: IS_REQUESTING, flag: false});
      console.log('Error on /auth/signin ' + error);
    });
  }


  return (
    <RootContext.Provider
      value={{
        isRequesting: state.isRequesting,
        isSuccess: state.isSuccess,
        isLogin: state.isLogin,
        error_message: state.error_message,
        userLogin: userLogin,
        userLogout: userLogout,
        register:register,
        isRegisterSuccess:state.isRegisterSuccess,
        user:state.user,
        reset:reset,
        sendEmail:sendEmail,
        isEmailSuccess:state.isEmailSuccess
      }}
    >
      {props.children}
    </RootContext.Provider>
  );
};

export default GlobalState;
