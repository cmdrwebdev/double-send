
export const RESET = 'RESET';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILED = 'USER_LOGIN_FAILED';

export const USER_REGISTER = 'USER_REGISTER';
export const USER_REGISTER_SUCCESS = 'USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAILED = 'USER_REGISTER_FAILED';

export const IS_REQUESTING = 'IS_REQUESTING';
export const IS_SUCCESS = 'IS_SUCCESS';
export const SEND_EMAIL = 'SEND_EMAIL';




const reset = (state) => {
  console.log("RESET " + state.isRegisterSuccess)
  return { ...state,
           error_message:"",
           isRegisterSuccess:false
  };
};
const userLogout = (state) => {
  return { ...state,user:{},isLogin: false, };
};
const userLogin = (user, state) => {
  return { ...state,user: user,error_message: "", isLogin: true };
};
const userLoginFailed = (message, state) => {
  return { ...state,error_message: message, isLogin: false };
};
const userRegisterSuccess = (flag,message, state) => {
  return { ...state, isRegisterSuccess: flag,error_message: message, isLogin: false };
};

const isRequesting = (flag, state) => {
  return { ...state, isRequesting: flag };
};
const isSuccess = (flag, state) => {
  return { ...state, isSuccess: flag };
};
const sendEmail = (flag,message,state) => {
  return { ...state, isEmailSuccess: flag,error_message:message };
};
export const rootReducer = (state, action) => {
  switch (action.type) {
    case RESET:
      return reset(state);
    case USER_LOGOUT:
      return userLogout(state);
    case USER_LOGIN:
      return userLogin(action.user, state);
    case USER_LOGIN_FAILED:
      return userLoginFailed(action.message, state);
    case USER_REGISTER_SUCCESS:
      return userRegisterSuccess(true,action.message,state);
    case USER_REGISTER_FAILED:
      return userRegisterSuccess(false,action.message,state);
    case IS_REQUESTING:
      return isRequesting(action.flag, state);
    case IS_SUCCESS:
      return isSuccess(action.flag, state);
    case SEND_EMAIL:
      return sendEmail(action.flag,action.message, state);
    default:
      return state;
  }
};
