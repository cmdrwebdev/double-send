import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";



import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import './css/freelancer.css';

import GlobalState from './context/GlobalState';
import HomePage from './pages/HomePage';
import SignupPage from './pages/SignupPage';
import SigninPage from './pages/SigninPage';
import RulesPage from './pages/RulesPage';
import TermsOfServicesPage from './pages/TermsOfServicePage';
import AboutPage from './pages/AboutPage';
import ContactPage from './pages/ContactPage';
import AppsPage from './pages/AppsPage';
import CongratulationsPage from './pages/CongratulationsPage';
import WelcomeBackPage from './pages/WelcomeBackPage';
import HowItWorksPage from './pages/HowItWorksPage';
import PrivacyPage from './pages/PrivacyPage';
const App = props => {
  return (
    <React.Fragment>
      <GlobalState>
      <div className="maincontainer"  >
        <div id="page-top">
          <Router>
            <Route path="/" exactly component={HomePage} exact />
            <Route path="/signup" component={SignupPage} />
            <Route path="/signin" component={SigninPage} />
            <Route path="/welcome" component={WelcomeBackPage} />
            <Route path="/rules" component={RulesPage} />
            <Route path="/tos" component={TermsOfServicesPage} />
            <Route path="/about" component={AboutPage} />
            <Route path="/contact" component={ContactPage} />
            <Route path="/app" component={AppsPage} />
            <Route path="/congratulations" component={CongratulationsPage} />
            <Route path="/how-it-works" component={HowItWorksPage} />
            <Route path="/privacy" component={PrivacyPage} />

          </Router>
          </div>
        </div>
      </GlobalState>
    </React.Fragment>
  );
};

export default App;
